# LiveChat

*Chat seeing other's messages while they type it*

## Install

Install pip and virtualenv for your OS. E.g. for ubuntu:

```
sudo apt install python-virtualenv python-pip
```

Create virtual environment in the local directory (recommended to isolate OS python from the project python):

```
virtualenv venv
```

Activate the environment (if you have created venv)

```
source venv/bin/activate
```

Install python requirements

```
pip install -r requirements.txt
```

Run the app:

```
python app.py
```
