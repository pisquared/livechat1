import os

from flask import Flask
from flask_socketio import SocketIO

basepath = os.path.dirname(os.path.realpath(__file__))
DB_PATH = os.path.join(basepath, "db.sqlite")


app = Flask(__name__)
app.config['SECURITY_PASSWORD_SALT'] = 'REPLACE_ME_WITH_SOMETHING_MORE_SECURE'
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_SEND_REGISTER_EMAIL'] = False
app.config['SECURITY_CONFIRMABLE'] = False
app.config['SECRET_KEY'] = 'REPLACE_ME_WITH_SOMETHING_MORE_SECURE'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(DB_PATH)
socketio = SocketIO(app,
                    # logger=True,
                    # engineio_logger=True,
                    cors_allowed_origins=[
                        "https://chat.pisquared.xyz",
                        "http://localhost:5000"]
                    )

DB = {}
