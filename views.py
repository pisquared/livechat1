from flask import render_template, request, session
from flask_security import login_required
from flask_socketio import emit

from app import app, socketio, DB
from models import Message, db


@app.route('/')
@login_required
def index():
    return render_template('index.html')


@socketio.on('connect')
def initial_connect():
    print("CONNECT", request.sid)
    if 'name' in session:
        DB[request.sid] = {'name': session['name']}
    commited = [m.serialize() for m in Message.query.all()]
    emit('srv_connect', {'DB': DB, 'COMMITED': commited})


@socketio.on('disconnect')
def initial_connect():
    print("DISCONNECT", request.sid)
    emit('client_disconnect', {'sid': request.sid})
    if request.sid in DB:
        del DB[request.sid]


@socketio.on('sid')
def handle_message(message):
    emit('sid', {'sid': request.sid})


@socketio.on('message_s')
def handle_message(message_envelope):
    DB[request.sid] = message_envelope
    session['name'] = message_envelope['name']
    emit('message_r', {'m': message_envelope, 'sid': request.sid}, broadcast=True)


@socketio.on('message_commit')
def handle_message_commit(message_envelope):
    message = Message(
        name=message_envelope.get('name'),
        message=message_envelope.get('message'),
        ts=message_envelope.get('ts'),
    )
    db.session.add(message)
    db.session.commit()
    emit('message_commit', message_envelope, broadcast=True)
